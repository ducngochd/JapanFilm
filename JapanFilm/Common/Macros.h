//
//  Macros.h
//  SevenBillionRun
//
//  Created by トラストリング on 2018/06/14.
//  Copyright © 2018 com.trustring.7billionrun. All rights reserved.
//

#ifndef Macros_h
#define Macros_h


#pragma mark DEFINE DATABASE

#define DATABASE_NAME                   @"sevenbillionrun"
#define TBL_VERSION_INFO                @"1.0"

//OLD
#define KEY_COCOLOLO_SDK                @"IkMJC5RXm"
#define KEY_COCOLOLO_MEASUREMENT                @"HuXJCc3RXe"

////NEW
//#define KEY_COCOLOLO_SDK                @"DkMJC5RXm"
//#define KEY_COCOLOLO_MEASUREMENT                @"DkMJC5RXm"



#define KEY_DATA            @"data"
#define KEY_RELATIVE        @"relative"
#define KEY_RELATIVE_INSERT         @"relativeInsert"
#define KEY_RESULT            @"result"
#define KEY_MEASURES            @"measures"
#define KEY_MEASURES2           @"measures2"
#define KEY_WEEK           @"week"
#define KEY_MONTH           @"month"
#define KEY_TOTAL           @"total"

#define USER_LOGIN_KEY          @"login"
#define USER_LOGIN_TOKEN          @"accesstoken"

#define URL_HELP_MEASUREMENT        @"http://cocololo.jp/app/helpactionselect.html"
#define URL_HELP_KYOUKAN        @"http://cocololo.jp/app/kimochinote/empathy/index.html"
#define URL_HELP_MEASUREMENT_RESULT        @"http://cocololo.jp/app/kimochinote/result/index.html"
#define PORT_TALK           @"6001"
#define BASE_URL                        @"http://api.vungoc.net/japanfilm/Film/"
#define API_APP(e) [BASE_URL stringByAppendingString:e]

//#define API_1                           @"api1"

#pragma mark - MACRO ENUM
enum
{
    kNoteLike        = 0,
    kNoteDisLike,
    kNoteTomorow
};

enum
{
    kInsertDiary        = 0,
    kUpdateDiary,
};

typedef enum {
    kPendingAccept = 0,
    kPendingNotAccept,
    kActionDefault,
    kActionEdit,
} kActionPending;

typedef enum {
    kEventDefault = 0,
    kEventEdit,
    kEventDelete,
} kEventState;

typedef enum {
    kAnswerDefault = 0,
    kAnswerFromHomeView,
    kAnswerFromQuestionView,
    kAnswerFromAnswerDetailView
}kAnswerTheQuestionState;

typedef enum {
    kElearning = 0,
    kElearningManabi
}kRecommendState;
#pragma mark - Google Analytics define

#define GOOGLE_ANALYTICS_HOMEVIEW                   @"ホーム"
#define GOOGLE_ANALYTICS_KIMOCHI_CATEGORIES         @"キモチをはかるカテゴリー"
#define GOOGLE_ANALYTICS_KIMOCHI_MESUREMENT         @"キモチをはかる中"
#define GOOGLE_ANALYTICS_KIMOCHI_RESULT             @"キモチをはかる結果"
#define GOOGLE_ANALYTICS_KIMOCHI_FINISH             @"キモチをはかる結局が表示"

#define GOOGLE_ANALYTICS_RELATIVE_MOOD              @"キモチを伝える"
#define GOOGLE_ANALYTICS_RELATIVE_DETAIL            @"キモチを伝える詳細"
#define GOOGLE_ANALYTICS_RELATIVE_CHAT              @"トーク"
#define GOOGLE_ANALYTICS_RELATIVE_ANSERTHEQUESTION              @"答えた問題"
#define GOOGLE_ANALYTICS_RELATIVE_THANKMESSAGE             @"感謝のメッセージ"
#define GOOGLE_ANALYTICS_RELATIVE_DETAIL_THANKMESSAGE       @"感謝のメッセージ詳細"
#define GOOGLE_ANALYTICS_RELATIVE_SEND_THANKMESSAGE         @"メッセージ作成"

#define GOOGLE_ANALYTICS_KOMOCHI_NOTE                       @"キモチノート"
#define GOOGLE_ANALYTICS_KIMOCHI_NOTE_MESSAGE               @"メッセージ集"
#define GOOGLE_ANALYTICS_KIMOCHI_NOTE_QUESTION             @"家族へ質問"
#define GOOGLE_ANALYTICS_KIMOCHI_NOTE_ANIVERSARY            @"家族のできること"
#define GOOGLE_ANALYTICS_KIMOCHI_NOTE_DIARY                 @"日記書"
#define GOOGLE_ANALYTICS_KIMOCHI_MESSAGE                    @"家族全員からのメッセージ"
#define GOOGLE_ANALYTICS_KIMOCHI_ADD_ANIVERSARY             @"記念日の追加"
#define GOOGLE_ANALYTICS_KIMOCHI_EDIT_ANIVERSARY             @"記念日の編集"
#define GOOGLE_ANALYTICS_KIMOCHI_DELETE_ANIVERSARY          @"記念日一覧"
#define GOOGLE_ANALYTICS_KIMOCHI_CATEGORIES_QUESTION        @"質問カテゴリー"
#define GOOGLE_ANALYTICS_KIMOCHI_DETAIL_QUESTION             @"質問詳細"
#define GOOGLE_ANALYTICS_KIMOCHI_ANSWER_QUESTION            @"質問回答"

#define GOOGLE_ANALYTICS_CONSULATION           @"悩みを相談"

#define GOOGLE_ANALYTICS_RECOMMENTD           @"おすすめコンテンツ"
#define GOOGLE_ANALYTICS_RECOMMEND_DETAIL       @"コンテンツ一覧"

#define YS_USERDEFAULT                  [YSUserDefault sharedUserDefault]

#define TITLE_NOTIFICATION_UPDATE_RELATION            @"通知"
#define TITLE_UPDATE_RELATION_SUCCESS                 @"より湾曲した"

#define CONSTANTS_RADIUS_BUTTON         5
#define CONSTANTS_BORDER_BUTTONCOLOR    @"a6caa9"
#define CONSTANTS_BACKGROUNG_BUTTON_TITLE   @"e7873b"

#define TITLE_HOMEVIEW                      @"角丸長アプリ"
#define TITLE_PEOPLEVIEW                    @"自分のこと"
#define TITLE_FAMILYVIEW                    @"家族のこと"
#define TITLE_FAMILY_RELATION_VIEW          @"家族を追加"
#define TITLE_FAMILY_NOTIFICATION_VIEW      @"家族の情報追加"
#define TITLE_MOOD_VIEW                     @"キモチを伝える"
#define TITLE_HEALTHY_VIEW                  @"キモチをはかる"
#define TITLE_MEMORIES_VIEW                  @"キモチノート"
#define TITLE_MESSAGE_VIEW                  @"メッセージ集"
#define TILE_MESSAGE_DETAID_VIEW            @"母からのメッセージ"

#define TITLE_MESSAGE_RECIVE                @"からのメッセージ"
#define TITLE_MESSAGE_RECIVE_CUSTOM(name) [name stringByAppendingString:TITLE_MESSAGE_RECIVE]
#define TITLE_MESSAGE_SEND                  @"への感謝のメッセージ"
#define TITLE_MESSAGE_SEND_CUSTOM(name) [name stringByAppendingString:TITLE_MESSAGE_SEND]
#define TITLE_SEND_MESSAGE_CUSTOM_NAME(s) [s stringByAppendingString:@"が答えた質問の答えは"]

#define TITLE_CONSULTATION_VIEW             @"相談する"
#define TITLE_RECOMMEND_VIEW                 @"おすすめコンテンツ"
#define TITLE_QUESTION_VIEW                 @"家族への質問"
#define TITLE_QUESTION_DETAIL_VIEW           @"質問カテゴリー"
#define TITLE_EVENT_FAMILY_VIEW              @"家族のできごと"
#define TITLE_NOTE_VIEW                  @"日記を書く"
#define TITLE_TALK_VIEW                      @"トーク"
#define TITLE_DETAIL_RELATION                @"家族を登録"
#define TITLE_SETTING_VIEW                   @"設定"
#define TITLE_POLICY_VIEW                    @"利用規約"
#define TITLE_BACKUP_VIEW                    @"データのバックアップ"
#define TITLE_DATA_BACKUP_VIEW               @"バックアップ登録"
#define TITLE_DATA_RESTORE_VIEW              @"データ引継ぎの方"

#define TITLE_MESURE_FINISH_VIEW                     @"測定終了"
#define TITLE_MESURE_RESULT_VIEW                     @"測定結果"
#define TITLE_CREATE_SEND_MESSAGE             @"メッセージ作成"

#define TITLE_POPUP_NOTIFICATION            @"お知らせ"
#define TITLE_UPLOAD_SUCCESS                @"保存しました。"

#define ARR_COLOR_RELATIVE              @[@"06344e",@"cc0707",@"1e485f",@"d11f1f",@"385d71",@"d63939",@"507183",@"db5151",@"6a8595",@"e06a6a",@"8299a6",@"e58383",@"9baeb8",@"eb9c9c"]


#define ARR_CONSULATION_TITLE           @[@"財産",@"相続",@"家族",@"夫婦",@"子ども",@"相談F",@"相談G",@"相談H",@"相談I",@"相談J"]
#define ARRAY_RELATIVE_TITLE           @[@"祖父",@"祖母",@"父",@"母",@"兄",@"姉",@"弟",@"妹",@"夫",@"妻",@"息子",@"娘",@"孫",@"その他"]

#define ARRAY_DETAIL_RELATIVE_TITLE           @[@"義祖父",@"義祖母",@"義父",@"義母",@"義兄",@"義姉",@"義弟",@"義妹",@"甥",@"姪"]

#define ARR_CONSULATION_IMAGE           @[@"ic_purse",@"ic_document",@"ic_family",@"ic_couple",@"ic_child",@"ic_note",@"ic_calender",@"ic_books",@"ic_purse",@"ic_document"]

#define ARRAY_HELTHY_TITLE           @[@"睡眠",@"身の回りの用事",@"食事",@"休養･くつろぎ",@"仕事",@"勉強",@"通勤・通学",@"移動",@"家事",@"介護・看護",@"育児",@"買い物",@"テレビ･ネット",@"スポーツ",@"趣味･娯楽",@"自己啓発",@"ボランティア",@"交際･付き合い",@"受診･療養",@"その他"]

#define ARRAY_CITY  @[@"北海道",@"青森県",@"宮城県",@"秋田県",@"山形県",@"福島県",@"茨城県",@"栃木県",@"群馬県",@"埼玉県",@"千葉県",@"東京都",@"神奈川県",@"新潟県",@"富山県",@"石川県",@"福井県",@"山梨県",@"長野県",@"岐阜県",@"静岡県",@"愛知県",@"三重県",@"滋賀県",@"京都府",@"大阪府",@"兵庫県",@"奈良県",@"和歌山県",@"鳥取県",@"島根県",@"岡山県",@"広島県",@"山口県",@"徳島県",@"香川県",@"愛媛県",@"高知県",@"福岡県",@"佐賀県",@"長崎県",@"熊本県",@"大分県",@"宮崎県",@"鹿児島県",@"沖縄県"]

#define ARR_BLOOD   @[@"A",@"B",@"AB",@"O"]



#define ERROR_SERVER_API        @"エラーが発生しました。"
#define ERROR_VALIDATE_EVENT_NAME   @"イベント該当者を選択してください。"
#define ERROR_VALIDATE_EVENT_DATE   @"イベントの日付を入力してください。"
#define ERROR_VALIDATE_EVENT_PEOPLE   @"イベントの送信者を選択してください。"
#define ERROR_VALIDATE_EVENT_ICON   @"イベントアイコンを選択してください。"
#define SUCCESS_INSERT_EVENT            @"イベントを正常に追加。"
#define ERROR_VALIDATE_MESSAGE_TITLE   @"タイトルメッセージを入力してください。"
#define ERROR_VALIDATE_MESSAGE_CONTENT   @"コンテンツメッセージを入力してください。"
#define ERROR_VALIDATE_MESSAGE          @"メッセージを入力してください。"

#define ERROR_VALIDATE_EMAIL            @"電子メールの形式が正しくありません。"
#define ERROR_TITLE_VALIDATE          @"お知らせ"

#define DURATION_TOAST_SECOND           1.5

#define SUCCESS_ADD_RELATIVE            @"新しい家族が追加されました！"


#define EVENT_ID                        @"event_id"
#define RELATIVE_DATE                   @"date"
#define RELATIVE_ICON_ID                @"icon_id"
#define RELATIVE_TITLE                  @"title"
#define RELATIVE_REPEAT                 @"repeat"

#define MESSAGE_USER_ID                 @"user_id"
#define MESSAGE_RELATIVE_ID             @"relative_id"
#define MESSAGE_CONTENT                 @"content"
#define MESSAGE_TYPE                    @"type"
#define MESSAGE_FILES                   @"files"
#define MESSAGE_CONTENT                 @"content"
#define MESSAGE_TIMMING                 @"timing"
#define MESSAGE_STRESS                  @"stress"
#define MESSAGE_EMPATHETIC              @"empathetic"
#define MESSAGE_ANIVERSARY_ID           @"aniversary_id"

#define REGISTER_USER_ID                @"user_id"
#define REGISTER_USER_NAME              @"username"
#define REGISTER_SEX                    @"sex"
#define REGISTER_NAME                   @"name"
#define REGISTER_HEIGHT                 @"height"
#define REGISTER_WEIGHT                 @"weight"
#define REGISTER_CITY                   @"city"
#define REGISTER_ADDRESS                @"address"
#define REGISTER_SALARY                 @"salary"
#define REGISTER_TOTAL_SALARY           @"total_salary"
#define REGISTER_ESTATE                 @"estate"

#define REGISTER_BLOODTYPE                 @"blood_type"

#define REGISTER_AVATAR_DATA            @"avatar"
#define REGISTER_AVATAR_NAME            @"nameAvatar"

#define REGISTER_BACKGROUND_DATA            @"background"
#define REGISTER_BACKGROUND_NAME            @"nameBackground"


#pragma mark IMAGE

#define FACE_FEELING_TOP           @"feeling_"
#define FACE_FEELING_0             @"feeling_0"
#define FACE_FEELING_1             @"feeling_1"
#define FACE_FEELING_2             @"feeling_2"
#define FACE_FEELING_3             @"feeling_3"
#define FACE_FEELING_4             @"feeling_4"
#define FACE_FEELING_5             @"feeling_5"
#define FACE_FEELING_6             @"feeling_6"
#define FACE_FEELING_7             @"feeling_7"
#define FACE_FEELING_NOTHING           @"face_nothing"

#define FACE_FEELING_WEB_0              @"feeling-web-0"
#define FACE_FEELING_WEB_1              @"feeling-web-1"
#define FACE_FEELING_WEB_2              @"feeling-web-2"
#define FACE_FEELING_WEB_3              @"feeling-web-3"
#define FACE_FEELING_WEB_4              @"feeling-web-4"
#define FACE_FEELING_WEB_5              @"feeling-web-5"
#define FACE_FEELING_WEB_6              @"feeling-web-6"
#define FACE_FEELING_WEB_7              @"feeling-web-7"



#define TITLE_FEELING_0             @"理想"
#define TITLE_FEELING_1             @"やや理想"
#define TITLE_FEELING_2             @"のんびり"
#define TITLE_FEELING_3             @"ややのんびり"
#define TITLE_FEELING_4             @"ぐったり"
#define TITLE_FEELING_5             @"ややぐったり"
#define TITLE_FEELING_6             @"ストレス気味"
#define TITLE_FEELING_7             @"ややストレス"

#define ICON_BACK_NAVIGATION            @"ic_arrow.png"
#define ICON_FACE_FEELING_1             @"ic_face_01"
#define ICON_FACE_FEELING_2             @"ic_face_03"
#define ICON_FACE_FEELING_3             @"ic_face_03"
#define ICON_FACE_FEELING_4             @"ic_face_01"

#define NO_IMAGE_AVATAR                 [UIImage imageNamed:@"person"]
#define BACKGROUND_IMAGE_COVER          [UIImage imageNamed:@"bg_header_question_top"]
#define ICON_SEND_STATUS_1              @"icon_mail_message"
#define ICON_SEND_STATUS_2              @"icon_message_note"



#pragma mark AVATAR

#define AVATAR_01                       @"avatar_01"
#define AVATAR_02                       @"avatar_02"
#define AVATAR_03                       @"avatar_03"
#define AVATAR_04                       @"avatar_04"


#pragma mark BACKGROUND

#define BACKGROUND_FEELING_0                        @"59b75b"
#define BACKGROUND_FEELING_1                        @"8bd48c"
#define BACKGROUND_FEELING_2                        @"f7ae00"
#define BACKGROUND_FEELING_3                        @"ffd46e"
#define BACKGROUND_FEELING_4                        @"34c5e9"
#define BACKGROUND_FEELING_5                        @"90e0f4"
#define BACKGROUND_FEELING_6                        @"ec7493"
#define BACKGROUND_FEELING_7                        @"eb9fb3"


#define DIARY_FEELING_0                             @"やったね！今日のキモチを忘れないように書いておこう♪"
#define DIARY_FEELING_1                             @"良い調子！明日もリラックスできるように書いてみてね♪"
#define DIARY_FEELING_2                             @"お疲れぎみかな？明日は今日より元気になれますように♪"
#define DIARY_FEELING_3                             @"少し元気ないみたい。日記を書いたらゆっくり休んでね♪"
#define DIARY_FEELING_4                             @"リフレッシュできたかな？ゆったりできた秘訣教えてね♪"
#define DIARY_FEELING_5                             @"穏やかに過ごせた1日だった？今日の事を書いてみよう♪"
#define DIARY_FEELING_6                             @"こんな日もあるね。全部書きだして心をリセットしよう♪"
#define DIARY_FEELING_7                             @"ちょっと頑張り気味だったかな？今日のこと書いてみて♪"


#define COLOR_BACKGROUND_ANSWER_DETAIL              @"fbfbfb"
#define BACKGROUND_CONSULATION_CELL                 @"eaa02b"
#define BACKGOUND_FAMILY_BUTTON_SEELECTED           @"eeeeee"

#define BACKGROUND_DIALOG_SELECTED                  @"ffe8bc"

#pragma mark COLOR
#define COLOR_FEELING_1                 @"d80000"
#define COLOR_BORDER_BUTTON             @"7eb582"
#define COLOR_BADGE_NOTIFI              @"d00707"
#define COLOR_BORDER_MESSAGE_VIEW       @"cccccc"
#define COLOR_BORDER_VIEW_DIALOG        @"f8ab3c"

#define FONT_HIRAKAKUPRO_W3 @"HiraKakuPro-W3"
#define FONT_HIRAKAKUPRO_W6 @"HiraKakuPro-W6"

#define STRESS_RESULT_TITLES          @[@"理想",@"やや理想",@"のんびり",@"ややのんびり",@"ぐったり",@"ややぐったり",@"ストレス気味",@"ややストレス"]
#define MAX_GUTTARI_TITLE @"かなりぐったり"
#define GUTTARI_TITLE @"ぐったり"
#define MAX_STRESS_TITLE @"かなりストレス"
#define STRESS_TITLE @"ストレス気味"

#define STRESS_RESULT_COLOR          @[@"59b75b",@"b2deb3",@"f7ae00",@"ffd46e",@"34c5e9",@"90e0f4",@"ec7493",@"f7c1cf"]

#define MEASURE_DATE_FORMAT @"yyyyMMdd.HHmmss"
#define CALENDARE_DATE_FORMAT @"yyyy年MM月dd日 HH:mm"
#define HEIGHT_NAVIGATIONBAR        64.0

#endif /* Macros_h */
