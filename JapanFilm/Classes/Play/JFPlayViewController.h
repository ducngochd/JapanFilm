//
//  JFPlayViewController.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/14.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "BaseViewController.h"

@interface JFPlayViewController : BaseViewController
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSDictionary *dict;
@end

