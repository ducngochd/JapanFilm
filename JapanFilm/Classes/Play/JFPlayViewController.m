//
//  JFPlayViewController.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/14.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "JFPlayViewController.h"
#import "YGPlayerView.h"
#import "YGPlayInfo.h"
#import "MJExtension.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>
@interface JFPlayViewController ()
@property (nonatomic, strong) NSMutableArray *playInfos;

@end

@implementation JFPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Play";
    [self setupPlayerView];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setupPlayerView
{
    YGPlayerView *playerView = [[[NSBundle mainBundle] loadNibNamed:@"YGPlayerView" owner:nil options:nil] lastObject];
    [self.view addSubview:playerView];
    YGPlayInfo *playInfo = [[YGPlayInfo alloc]init];
    [[XCDYouTubeClient defaultClient] getVideoWithIdentifier:[NSString extractYoutubeIdFromLink:@"https://www.youtube.com/watch?v=OoXWhZ7GKVY"] completionHandler:^(XCDYouTubeVideo *video, NSError *error) {
        if (video) {
            NSDictionary *streamURLs = video.streamURLs;
            NSURL *url = streamURLs[XCDYouTubeVideoQualityHTTPLiveStreaming] ?: streamURLs[@(XCDYouTubeVideoQualityHD720)] ?: streamURLs[@(XCDYouTubeVideoQualityMedium360)] ?: streamURLs[@(XCDYouTubeVideoQualitySmall240)];
            playInfo.url = [url absoluteString];
            [playerView playWithPlayInfo:playInfo];

        }
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
