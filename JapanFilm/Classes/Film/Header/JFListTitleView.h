//
//  JFListTitleView.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/18.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JFFilmEntity.h"

@interface JFListTitleView : UIView
@property (nonatomic, strong) UIView *tagView;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIButton *moreButton;

@property (nonatomic, strong) JFFilmEntity *contentModel;

@end
