//
//  JFListTitleView.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/18.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "JFListTitleView.h"
@implementation JFListTitleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews{
    [self addSubview:self.contentLabel];
    [self addSubview:self.tagView];
    [self addSubview:self.moreButton];
}

- (UIView *)tagView{
    if (!_tagView) {
        _tagView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, 5, 15)];
        _tagView.backgroundColor = UIColorFromRGB(0xffbf00);
        _tagView.layer.masksToBounds = YES;
        _tagView.layer.cornerRadius = 2.5;
    }
    return _tagView;
}

- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(25, 0, SCREEN_WIDTH, self.frame.size.height)];
        _contentLabel.font = [UIFont systemFontOfSize:13];
        _contentLabel.textColor = UIColorFromRGB(0x666666);
        _contentLabel.layer.zPosition = -1;
        self.contentLabel.text = @"Content";

    }
    return _contentLabel;
}

- (UIButton *)moreButton{
    if (!_moreButton) {
        _moreButton = [[UIButton alloc]initWithFrame:CGRectMake(self.frame.size.width - 100 - 10, 0, 100, self.frame.size.height)];
        _moreButton.titleLabel.font = [UIFont systemFontOfSize:11];
        _moreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_moreButton setTitleColor:UIColorFromRGB(0xBBBBBB) forState:UIControlStateNormal];
        [_moreButton addTarget:self action:@selector(moreButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [_moreButton setTitle:@"More" forState:UIControlStateNormal];
    }
    return _moreButton;
}

- (void)setContentModel:(JFFilmEntity *)contentModel{
    _contentModel = contentModel;
    self.contentLabel.text = contentModel.name;

}

@end
