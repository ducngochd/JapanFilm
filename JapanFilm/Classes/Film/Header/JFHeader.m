//
//  JFHeader.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/14.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "JFHeader.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CWCarousel.h"
#import "CWPageControl.h"
#define kViewTag 666
#define kCount 5


@interface JFHeader ()<CWCarouselDatasource, CWCarouselDelegate> {
    CGRect frameView;
}
@property (nonatomic, strong) CWCarousel *carousel;
@property (nonatomic, strong) UIView *animationView;


@end
@implementation JFHeader

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        frameView = frame;
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.animationView];
        [self configureUI];
    }
    return self;
}


- (void)configureUI {
    CATransition *tr = [CATransition animation];
    tr.type = @"cube";
    tr.subtype = kCATransitionFromRight;
    tr.duration = 0.25;
    [self.animationView.layer addAnimation:tr forKey:nil];
    
    if(self.carousel) {
        [self.carousel removeFromSuperview];
        self.carousel = nil;
    }
    
    self.animationView.backgroundColor = [UIColor whiteColor];
    CWFlowLayout *flowLayout = [[CWFlowLayout alloc] initWithStyle:CWCarouselStyle_H_2];
    CWCarousel *carousel = [[CWCarousel alloc] initWithFrame:self.animationView.bounds
                                                    delegate:self
                                                  datasource:self
                                                  flowLayout:flowLayout];
    carousel.isAuto = YES;
    carousel.backgroundColor = [UIColor whiteColor];

    CWPageControl *control = [[CWPageControl alloc] initWithFrame:CGRectMake(0, 0, 300, 5)];
    control.center = CGPointMake(CGRectGetWidth(carousel.frame) * 0.5, CGRectGetHeight(carousel.frame) - 10);
    control.pageNumbers = 5;
    control.currentPage = 0;
    carousel.customPageControl = control;
    [self.animationView addSubview:carousel];
    [carousel registerViewClass:[UICollectionViewCell class] identifier:@"cellId"];
    [carousel freshCarousel];
    self.carousel = carousel;
}

- (NSInteger)numbersForCarousel {
    return kCount;
}

#pragma mark - Delegate
- (UICollectionViewCell *)viewForCarousel:(CWCarousel *)carousel indexPath:(NSIndexPath *)indexPath index:(NSInteger)index{
    UICollectionViewCell *cell = [carousel.carouselView dequeueReusableCellWithReuseIdentifier:@"cellId" forIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor cyanColor];
    UIImageView *imgView = [cell.contentView viewWithTag:kViewTag];
    if(!imgView) {
        imgView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
        imgView.tag = kViewTag;
        imgView.backgroundColor = [UIColor redColor];
        [cell.contentView addSubview:imgView];
        cell.layer.masksToBounds = YES;
        cell.layer.cornerRadius = 8;
    }
    NSString *name = [NSString stringWithFormat:@"%02ld.jpg", index + 1];
    UIImage *img = [UIImage imageNamed:name];
    if(!img) {
        NSLog(@"%@", name);
    }
    [imgView setImage:img];
    return cell;
}

- (void)CWCarousel:(CWCarousel *)carousel didSelectedAtIndex:(NSInteger)index {
    [self.delegate clickImage];
    NSLog(@"...%ld...", index);
}

- (UIView *)animationView{
    if(!_animationView) {
        self.animationView = [[UIView alloc] initWithFrame:CGRectMake(0, (frameView.size.height - (frameView.size.height - 50))/2, CGRectGetWidth(self.frame), self.frame.size.height-50)];
    }
    return _animationView;
}



@end
