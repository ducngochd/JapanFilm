//
//  JFHeader.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/14.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JFPlayViewController.h"

@protocol JFHeaderDelegate <NSObject>
- (void)clickImage;
@end
@interface JFHeader : UICollectionReusableView
@property (weak, nonatomic) id<JFHeaderDelegate> delegate;
@end
