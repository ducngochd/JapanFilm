//
//  JFTypeFilmCell.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/18.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "JFTypeFilmCell.h"
#import "JFComicBlockingContentCellCollectionViewCell.h"
static NSString *blockingCellIdentifier = @"JFComicBlockingContentCellCollectionViewCellIdentifier";

@implementation JFTypeFilmCell
- (void)initCompletionOpration{
    [self.contentCollectionView registerNib:[UINib nibWithNibName:@"JFComicBlockingContentCellCollectionViewCell" bundle:nil]
                 forCellWithReuseIdentifier:blockingCellIdentifier];
    self.contentLayout.minimumLineSpacing = 10;
    self.contentLayout.minimumInteritemSpacing = 10;
    self.contentLayout.sectionInset = UIEdgeInsetsMake(5, 10, 5, 10);
}

- (void)contentModelCompletionOpration{
    self.contentLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.contentCollectionView.scrollEnabled = YES;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat height = (self.contentCollectionView.frame.size.height - 20)/ 2;
    return CGSizeMake(height/ 90.0 * 130, height);

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JFComicBlockingContentCellCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:blockingCellIdentifier forIndexPath:indexPath];
    NSLog(@"dem %ld",self.contentModelArray.count);
    if (self.contentModelArray.count > 0) {
        cell.modelFilm = self.contentModelArray[indexPath.row];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    JFFilmContentEntity *film = self.contentModelArray[indexPath.row];
    JFPlayViewController *playVC = [UI_STORYBOARD instantiateViewControllerWithIdentifier:@"JFPlayViewController"];
    playVC.url = film.url;
    [[JFControllerManager shared].navigation pushViewController:playVC animated:YES];
}
@end
