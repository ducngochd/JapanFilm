//
//  JFBaseCell.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/18.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JFListTitleView.h"
#import "JFFilmEntity.h"
#import "JFFilmContentEntity.h"
@interface JFBaseCell : UICollectionViewCell <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) JFListTitleView *headerView;
@property (nonatomic, strong) JFFilmEntity *contentModel;
@property (nonatomic, strong) JFFilmContentEntity *modelFilm;
@property (nonatomic, strong) NSMutableArray *contentModelArray;
@property (nonatomic, strong) UICollectionView *contentCollectionView;
@property (nonatomic, weak) UICollectionViewFlowLayout *contentLayout;


- (void)initCompletionOpration;
- (void)contentModelCompletionOpration;

@end
