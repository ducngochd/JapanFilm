//
//  JFTypeFilmCell.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/18.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "JFBaseCell.h"
#import "JFFilmContentEntity.h"
#import "JFPlayViewController.h"
#import "JFControllerManager.h"
@protocol JFTypeFilmCellDelegate <NSObject>
- (void)didSelectFilm:(JFFilmContentEntity *)film;
@end
@interface JFTypeFilmCell : JFBaseCell
@property (weak, nonatomic) id<JFTypeFilmCellDelegate> delegate;
@end
