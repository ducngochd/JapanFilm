//
//  JFFilmViewController.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/14.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "JFFilmViewController.h"
#import "JFHeader.h"
//#import "CommonTool.h"
#import "JFPlayViewController.h"
#import "JFTypeFilmCell.h"
#import <FirebaseDatabase/FIRDatabase.h>
#import "JFFilmEntity.h"
#import "JFFilmContentEntity.h"
#import "NVNetworkManager.h"
#import "JFControllerManager.h"
#import "AFNet.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>
#import "JFObjectResponse.h"
static NSString *headerIdentifier = @"HeaderIdentifier";


@interface JFFilmViewController ()<JFHeaderDelegate,UICollectionViewDelegate,UICollectionViewDataSource> {
    NSDictionary *dict;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) JFTypeFilmCell *filmCell;
@property (nonatomic, strong) NSMutableArray *listRowContent;
@property (nonatomic, strong) NSMutableArray *listModelArray;
@property (strong, nonatomic) FIRDatabaseReference *ref;

@end

@implementation JFFilmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"Film"];
    self.ref = [[FIRDatabase database] reference];
    dict = [NSDictionary new];
    [self getDataFromServer];
    [self.collectionView registerClass:[JFHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerIdentifier];
//    [CommonTool registerCellForCollectionView:self.collectionView];
    
    [self.collectionView registerClass:[JFTypeFilmCell class] forCellWithReuseIdentifier:@"JFTypeFilmCell"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"qua roi");
}

#pragma mark - 数据源
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    NewStoreTitleModel *model = _listRowContentModelArray[indexPath.row];
    NSString *identifier = @"JFTypeFilmCell";

    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//    cell.listFilm = listFilm;
    JFFilmEntity *film = self.listModelArray[indexPath.row];
    if ([cell respondsToSelector:@selector(setContentModel:)]) {
        [cell setValue:film forKey:@"contentModel"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.listModelArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(SCREEN_WIDTH, 35 + 245 * SCREEN_WIDTH/375.0 );
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    JFHeader *collectionReusableView = nil;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        collectionReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:headerIdentifier forIndexPath:indexPath];
        collectionReusableView.delegate = self;
        
//        collectionReusableView.modelArray = self.headerModelArray;
    }
    return collectionReusableView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(SCREEN_WIDTH, SCREEN_WIDTH/375.0 * 235.0);
}

- (void)clickImage {
    
    JFPlayViewController *playVC = [UI_STORYBOARD instantiateViewControllerWithIdentifier:@"JFPlayViewController"];
    [[XCDYouTubeClient defaultClient] getVideoWithIdentifier:[NSString extractYoutubeIdFromLink:@"https://www.youtube.com/watch?v=OoXWhZ7GKVY"] completionHandler:^(XCDYouTubeVideo *video, NSError *error) {
        if (video) {
            NSDictionary *streamURLs = video.streamURLs;
            NSURL *url = streamURLs[XCDYouTubeVideoQualityHTTPLiveStreaming] ?: streamURLs[@(XCDYouTubeVideoQualityHD720)] ?: streamURLs[@(XCDYouTubeVideoQualityMedium360)] ?: streamURLs[@(XCDYouTubeVideoQualitySmall240)];
            playVC.url = [url absoluteString];
            [[JFControllerManager shared].navigation pushViewController:playVC animated:YES];
        }
    }];
//    [self.navigationController pushViewController:playVC animated:YES];
    
}

- (void)getDataFromServer {
    [AFNet requestWithUrl:API_APP(@"film") requestType:HttpRequestTypeGet parameter:nil completation:^(id object) {
        NSLog(@"%@",object);
        JFObjectResponse *response = [JFObjectResponse modelWithDictionary:object];
        self.listModelArray = [NSMutableArray arrayWithArray:response.listObject];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
//    [NVNetworkManager getRequestWithURL:@"http://api.vungoc.net/japanfilm/Film/film" params:nil successBlock:^(NSDictionary *returnResponse) {
//        if ([returnResponse objectForKey:@"status"]) {
//
//        }
//    } failureBlock:^(NSError *error) {
//        NSLog(@"%@",error);
//    } showLoading:NO];
}

- (void)requestDataForContentListModel:(JFFilmEntity *)listModel {
    [AFNet requestWithUrl:API_APP(@"film") requestType:HttpRequestTypeGet parameter:@{@"film_id":[NSNumber numberWithInteger:listModel.film_id]} completation:^(id object) {
        NSLog(@"%@", object);
        NSMutableArray *array = [JFFilmContentEntity modelArrayForDataArray:[object objectForKey:@"data"]];
        listModel.contentArray = array;
        [self.collectionView reloadData];
    } failure:^(NSError *error) {
        
    }];
}


#pragma mark - GET SET

- (void)setListModelArray:(NSMutableArray *)listModelArray {
    _listModelArray = listModelArray;
    for (JFFilmEntity *listModel in self.listModelArray) {
        [self requestDataForContentListModel:listModel];
    }

}

- (void)setListRowContent:(NSMutableArray *)listRowContent {
    _listRowContent = listRowContent;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
