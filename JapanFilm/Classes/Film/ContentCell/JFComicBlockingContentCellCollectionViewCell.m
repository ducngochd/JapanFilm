//
//  JFComicBlockingContentCellCollectionViewCell.m
//  ComicReader
//
//  Created by Mr_J on 16/4/30.
//  Copyright © 2016年 Mac. All rights reserved.
//

#import "JFComicBlockingContentCellCollectionViewCell.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@implementation JFComicBlockingContentCellCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}

- (void)setModelFilm:(JFFilmContentEntity *)modelFilm {
        _modelFilm = modelFilm;
    [[XCDYouTubeClient defaultClient] getVideoWithIdentifier:[NSString extractYoutubeIdFromLink:modelFilm.url] completionHandler:^(XCDYouTubeVideo *video, NSError *error) {
        if (video) {
            [self.contentImageView sd_setImageWithURL:video.smallThumbnailURL];
        }
    }];
        self.titleLabel.text = modelFilm.text;

}
- (void)setContentModel:(NSArray *)array{
//    _contentModel = contentModel;
//    [self.contentImageView sd_setImageWithURL:[NSURL URLWithString:contentModel.cover_image_url]];
//    self.titleLabel.text = contentModel.title;
}

@end
