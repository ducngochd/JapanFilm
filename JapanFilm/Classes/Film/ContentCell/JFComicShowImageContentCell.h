//
//  JFComicShowImageContentCell.h
//  ComicReader
//
//  Created by Mr_J on 16/4/30.
//  Copyright © 2016年 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JFComicShowImageContentCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *contentImageView;


@end
