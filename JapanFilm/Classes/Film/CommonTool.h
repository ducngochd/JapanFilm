//
//  CommonTool.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/14.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface CommonTool : NSObject
+ (NSArray *)arrayForListCellClassName;

+ (void)registerCellForCollectionView:(UICollectionView *)collectionView;

@end
