//
//  CommonTool.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/14.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "CommonTool.h"

@implementation CommonTool
+ (NSArray *)arrayForListCellClassName{
    return @[@"JFComicShowContentCollectionView",
             @"JFComicBlockingCollectionView",
             @"JFComicShowImageCollectionView"];
}

+ (void)registerCellForCollectionView:(UICollectionView *)collectionView{
    NSArray *classArray = [self arrayForListCellClassName];
    
    for (NSString *className in classArray) {
        Class class = NSClassFromString(className);
        NSString *identifier = [NSString stringWithFormat:@"%@", className];
        [collectionView registerClass:class forCellWithReuseIdentifier:identifier];
    }
}
@end
