//
//  JFCustomNavigationViewController.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/24.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "JFCustomNavigationViewController.h"

@interface JFCustomNavigationViewController ()

@end

@implementation JFCustomNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBar.topItem.title = @"";
    self.navigationBar.tintColor = [UIColor blackColor];

    [self.navigationBar setBackgroundImage:[self resizableImage:@"header_bg_message" edgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.translucent = NO;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
}

- (UIImage *)resizableImage:(NSString *)imageName edgeInsets:(UIEdgeInsets)edgeInsets {
    UIImage *image = [UIImage imageNamed:imageName];
    
    if (!image) {
        return nil;
    }
    CGFloat imageW = image.size.width;
    CGFloat imageH = image.size.height;
    
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(imageH*edgeInsets.top, imageW*edgeInsets.left, imageH*edgeInsets.bottom, imageW*edgeInsets.right) resizingMode:UIImageResizingModeStretch];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
