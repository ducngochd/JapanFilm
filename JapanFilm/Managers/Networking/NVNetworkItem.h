//
//  NVNetworkItem.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/19.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NVNetworkDefine.h"
#import "NVNetworkDelegate.h"
@interface NVNetworkItem : NSObject

@property (nonatomic, assign) NVNetworkType networkType;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSDictionary *params;
@property (nonatomic, assign) id<NVNetworkDelegate> delegate;

@property (nonatomic,assign) id tagrget;

@property (nonatomic,assign) SEL select;

@property (nonatomic, assign) BOOL showLoading;

- (NVNetworkItem *)initWithType:(NVNetworkType)networkType
                            url:(NSString *)url
                         params:(NSDictionary *)params
                       delegate:(id)delegate
                         target:(id)target
                         action:(SEL)action
                      hashValue:(NSUInteger)hashValue
                    showLoading:(BOOL)showLoading
                   successBlock:(NVSuccessBlock)successBlock
                   failureBlock:(NVFailureBlock)failureBlock;
@end
