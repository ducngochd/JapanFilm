//
//  NVNetworkItem.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/19.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "NVNetworkItem.h"
#import "AFNetworking.h"
#import "MBProgressHUD+Add.h"
@implementation NVNetworkItem

- (NVNetworkItem *)initWithType:(NVNetworkType)networkType
                            url:(NSString *)url
                         params:(NSDictionary *)params
                       delegate:(id)delegate
                         target:(id)target
                         action:(SEL)action
                      hashValue:(NSUInteger)hashValue
                    showLoading:(BOOL)showLoading
                   successBlock:(NVSuccessBlock)successBlock
                   failureBlock:(NVFailureBlock)failureBlock {
    if (self = [super init]) {
        self.networkType = networkType;
        self.url = url;
        self.params = params;
        self.delegate = delegate;
        self.showLoading = showLoading;
        self.tagrget = target;
        self.select = action;
        if (showLoading) {
            [MBProgressHUD showHUDAddedTo:nil animated:YES];
        }
        __weak typeof(self)weakSelf = self;
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", nil];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
    
        if (networkType == NVNetworkGET) {
            [manager GET:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                [MBProgressHUD hideAllHUDsForView:nil animated:YES];
                successBlock(responseObject);
                if ([weakSelf.delegate respondsToSelector:@selector(requestDidFinishLoading:)]) {
                    [weakSelf.delegate requestDidFinishLoading:responseObject];
                }
                [weakSelf performSelector:@selector(finishedRequest:didFaild:) withObject:responseObject withObject:nil];
                [weakSelf removeItem];

            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [MBProgressHUD hideAllHUDsForView:nil animated:YES];
                failureBlock(error);
                if ([weakSelf.delegate respondsToSelector:@selector(requestdidFailWithError:)]) {
                    [weakSelf.delegate requestdidFailWithError:error];
                }
                [weakSelf performSelector:@selector(finishedRequest:didFaild:) withObject:nil withObject:error];
                [weakSelf removeItem];
            }];
        }
        
    }
    return self;
}

- (void)removeItem
{
    __weak typeof(self)weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([weakSelf.delegate respondsToSelector:@selector(netWorkWillDealloc:)]) {
            [weakSelf.delegate netWorkWillDealloc:weakSelf];
        }
    });
}

- (void)finishedRequest:(id)data didFaild:(NSError*)error
{
    if ([self.tagrget respondsToSelector:self.select]) {
        [self.tagrget performSelector:@selector(finishedRequest:didFaild:) withObject:data withObject:error];
    }
}

@end
