//
//  NVNetworkManager.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/19.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NVNetworkDefine.h"
#import "NVNetworkDelegate.h"
@interface NVNetworkManager : NSObject
+(instancetype)sharedInstance;

+ (void)getRequestWithURL:(NSString*)url
                  params:(NSDictionary*)paramsDict
            successBlock:(NVSuccessBlock)successBlock
            failureBlock:(NVFailureBlock)failureBlock
                 showLoading:(BOOL)showLoading;
@end
