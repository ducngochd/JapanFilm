//
//  NVNetworkManager.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/19.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "NVNetworkManager.h"
#import "NVNetworkHandler.h"
@implementation NVNetworkManager

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    static NVNetworkManager *_manager;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _manager = [super allocWithZone:zone];
    });
    return _manager;
}
+(instancetype)sharedInstance
{
    return [[super alloc] init];
}

+ (void)getRequestWithURL:(NSString*)url
                  params:(NSDictionary*)params
                  target:(id)target
                  action:(SEL)action
                delegate:(id)delegate
            successBlock:(NVSuccessBlock)successBlock
            failureBlock:(NVFailureBlock)failureBlock
                 showLoading:(BOOL)showLoading
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionaryWithDictionary:params];
    [[NVNetworkHandler sharedInstance] conURL:url networkType:NVNetworkGET params:mutableDict delegate:delegate showLoading:showLoading target:target action:action successBlock:successBlock failureBlock:failureBlock];
}

+ (void)getRequestWithURL:(NSString *)url
                   params:(NSDictionary *)paramsDict
             successBlock:(NVSuccessBlock)successBlock
             failureBlock:(NVFailureBlock)failureBlock
              showLoading:(BOOL)showLoading
{
    [self getRequestWithURL:url params:paramsDict target:nil action:nil delegate:nil successBlock:successBlock failureBlock:failureBlock showLoading:showLoading];
}
@end
