//
//  NVNetworkHandler.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/19.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NVNetworkDefine.h"

@class NVNetworkItem;
@interface NVNetworkHandler : NSObject

+ (NVNetworkHandler *)sharedInstance;

@property(nonatomic,strong)NSMutableArray *items;
@property(nonatomic,strong) NVNetworkItem *netWorkItem;

@property(nonatomic,assign)BOOL networkError;

- (NVNetworkItem*)conURL:(NSString *)url
                networkType:(NVNetworkType)networkType
                     params:(NSMutableDictionary *)params
                   delegate:(id)delegate
                    showLoading:(BOOL)showLoading
                     target:(id)target
                     action:(SEL)action
               successBlock:(NVSuccessBlock)successBlock
               failureBlock:(NVFailureBlock)failureBlock;


+ (void)startMonitoring;

+ (void)cancelAllNetItems;


@end
