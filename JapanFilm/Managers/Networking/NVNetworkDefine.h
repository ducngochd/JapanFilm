//
//  NVNetworkDefine.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/19.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#ifndef NVNetworkDefine_h
#define NVNetworkDefine_h

typedef enum {
    NVNetworkGET = 1,
    NVNetworkPOST,
    NVNetworkPUT,
    NVNetworkDELETE,
} NVNetworkType;

#define NV_REQUEST_TIME_OUT 20

#if NS_BLOCKS_AVAILABLE

typedef void(^NVStartBlock)(void);


typedef void(^NVSuccessBlock)(NSDictionary *returnResponse);


typedef void(^NVFailureBlock)(NSError *error);

#endif
#endif /* NVNetworkDefine_h */
