//
//  NVNetworkHandler.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/19.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "NVNetworkHandler.h"
#import "NVNetworkItem.h"
#import "AFNetworking.h"

@interface NVNetworkHandler ()<NVNetworkDelegate>
@end


@implementation NVNetworkHandler

+ (NVNetworkHandler *)sharedInstance
{
    static NVNetworkHandler *handler = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        handler = [[NVNetworkHandler alloc] init];
    });
    return handler;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.networkError = NO;
    }
    return self;
}

- (NVNetworkItem*)conURL:(NSString *)url
                networkType:(NVNetworkType)networkType
                     params:(NSMutableDictionary *)params
                   delegate:(id)delegate
                    showLoading:(BOOL)showLoading
                     target:(id)target
                     action:(SEL)action
               successBlock:(NVSuccessBlock)successBlock
               failureBlock:(NVFailureBlock)failureBlock
{
    if (self.networkError == YES) {
//        SHOW_ALERT(@"网络连接断开,请检查网络!");
        if (failureBlock) {
            failureBlock(nil);
        }
        return nil;
    }
    /// 如果有一些公共处理，可以写在这里
    NSUInteger hashValue = [delegate hash];
    self.netWorkItem = [[NVNetworkItem alloc]initWithType:networkType url:url params:params delegate:delegate target:target action:action hashValue:hashValue showLoading:showLoading successBlock:successBlock failureBlock:failureBlock];
    self.netWorkItem.delegate = self;
    [self.items addObject:self.netWorkItem];
    return self.netWorkItem;
}


+ (void)startMonitoring
{
    AFNetworkReachabilityManager *mgr = [AFNetworkReachabilityManager sharedManager];
    [mgr setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status)
        {
            case AFNetworkReachabilityStatusUnknown:
                [NVNetworkHandler sharedInstance].networkError = NO;
                break;
            case AFNetworkReachabilityStatusNotReachable:
                [NVNetworkHandler sharedInstance].networkError = YES;
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                [NVNetworkHandler sharedInstance].networkError = NO;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                [NVNetworkHandler sharedInstance].networkError = NO;
                break;
        }
    }];
    [mgr startMonitoring];
}

- (NSMutableArray *)items
{
    if (!_items) {
        _items = [NSMutableArray array];
    }
    return _items;
}


+ (void)cancelAllNetItems
{
    NVNetworkHandler *handler = [NVNetworkHandler sharedInstance];
    [handler.items removeAllObjects];
    handler.netWorkItem = nil;
}

- (void)netWorkWillDealloc:(NVNetworkItem *)item
{
    [self.items removeObject:item];
    self.netWorkItem = nil;
}

@end
