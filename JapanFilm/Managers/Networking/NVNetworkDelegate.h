//
//  NVNetworkDelegate.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/19.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NVNetworkItem;

@protocol NVNetworkDelegate <NSObject>
@optional
- (void)requestDidFinishLoading:(NSDictionary*)returnResponse;
- (void)requestdidFailWithError:(NSError*)error;
- (void)netWorkWillDealloc:(NVNetworkItem*)item;

@end

