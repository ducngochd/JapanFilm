//
//  AFNet.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/19.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "AFHTTPSessionManager.h"
typedef NS_ENUM(NSInteger , HttpRequestType) {
    HttpRequestTypeGet = 0,
    HttpRequestTypePost,
};

typedef void(^successBlock)(id object);
typedef void(^failureBlock)(NSError *error);

@interface AFNet : NSObject

+ (void)requestWithUrl:(NSString *)url requestType:(HttpRequestType)requestType parameter:(NSDictionary *)parameter completation:(successBlock)success failure:(failureBlock)failure;
@end
