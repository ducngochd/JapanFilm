//
//  AFNet.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/19.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "AFNet.h"

@implementation AFNet

+ (id)shareManager {
    static AFHTTPSessionManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [AFHTTPSessionManager manager];
        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy defaultPolicy];
        securityPolicy.allowInvalidCertificates = NO;
        securityPolicy.validatesDomainName = YES;
        manager.securityPolicy  = securityPolicy;
        
        manager.requestSerializer.timeoutInterval = 12.f;
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json",@"text/html",@"text/plain",@"text/javascript",nil];
    });
    return manager;
}

+ (void)requestWithUrl:(NSString *)url requestType:(HttpRequestType)requestType parameter:(NSDictionary *)parameter completation:(successBlock)success failure:(failureBlock)failure {
    [self checkNetworkReachabilityStatus];
    AFHTTPSessionManager *sessionManager = [AFNet shareManager];
    [self setNetworkActivityIndicator:YES];
    if (requestType == HttpRequestTypeGet) {
        [sessionManager GET:url parameters:parameter success:^(NSURLSessionDataTask *task, id responseObject) {
            id responseData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            if (success) {
                success(responseData);
            }
            [self setNetworkActivityIndicator:NO];

        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self setNetworkActivityIndicator:NO];
            if (failure) {
                failure(error);
            }
        }];
    } else {
        [sessionManager POST:url parameters:parameter success:^(NSURLSessionDataTask *task, id responseObject) {
            id responseData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            if (!responseData) {
                return;
            }
            if (success && responseData) {
                success(responseData);
            }
            [self setNetworkActivityIndicator:NO];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self setNetworkActivityIndicator:NO];
            if (failure) {
                failure(error);
            }
        }];
    }
}

+ (void)checkNetworkReachabilityStatus{
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:{
                NSLog(@"WAN");
                break;
            }
            case AFNetworkReachabilityStatusReachableViaWiFi:{
                NSLog(@"wifi");
                break;
            }
            case AFNetworkReachabilityStatusNotReachable:
            case AFNetworkReachabilityStatusUnknown:{
                NSLog(@"Unknow");
                return;
                break;
            }
            default:
                break;
        }
        
    }];
}

+ (void)setNetworkActivityIndicator:(BOOL)sign {
    UIApplication *app = [UIApplication sharedApplication];
    [app setNetworkActivityIndicatorVisible:sign];
}
@end
