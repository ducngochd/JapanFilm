//
//
//  JapanFilm
//
//  Created by トラストリング on 2018/07/19.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YGVideoTool : NSObject
+ (instancetype)sharedVideoTool;
+ (NSMutableArray *)playInfos;
@end
