//
//  JFControllerManager.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/17.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JFControllerManager : NSObject
@property (strong, nonatomic) UINavigationController *navigation;
+ (instancetype)shared;

@end

