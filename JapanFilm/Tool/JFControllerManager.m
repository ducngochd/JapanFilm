//
//  JFControllerManager.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/17.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "JFControllerManager.h"
static JFControllerManager *controllerManager;

@implementation JFControllerManager
+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        controllerManager = [super allocWithZone:zone];
    });
    return controllerManager;
}

+ (void)load{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        controllerManager = [[self alloc]init];
    });
}

+ (instancetype)shared{
    return controllerManager;
}

- (UINavigationController *)navigation{
    if (!_navigation) {
        _navigation = (UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    }
    return _navigation;
}
@end
