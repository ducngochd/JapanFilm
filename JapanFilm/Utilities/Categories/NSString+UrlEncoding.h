

#import <Foundation/Foundation.h>

@interface NSString (NSString_UrlEncoding)

-(NSString*)urlEncodedString;

+ (NSString *)extractYoutubeIdFromLink:(NSString *)link;
+ (NSString *)getLocalAppVersion;
+ (NSString *)getBundleID;
+ (NSString *)getBuildVersion;
+ (NSString *)getPhoneVersion;
@end
