
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSDictionary (NSDictionary_SafeValues)

- (NSString*)safeStringForKey:(id)key;
- (NSNumber*)safeNumberForKey:(id)key;
- (NSArray*)safeArrayForKey:(id)key;
- (NSDictionary*)safeDictionaryForKey:(id)key;
- (UIImage*)safeImageForKey:(id)key;
- (id)safeObjectForKey:(id)key;
@end
