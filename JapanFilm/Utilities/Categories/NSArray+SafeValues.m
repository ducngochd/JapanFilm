

#import "NSArray+SafeValues.h"

@implementation NSArray (NSArray_SafeValues)

- (NSString*)safeStringAtIndex:(NSUInteger)index {
    NSString* string = nil;
    
    if (index < self.count){
        id obj = [self objectAtIndex:index];
        if ([obj isKindOfClass:[NSString class]]){
            string = obj;
        }
    }
    return string;
}

- (NSNumber*)safeNumberAtIndex:(NSUInteger)index {
    NSNumber* number = nil;
    
    if (index < self.count){
        id obj = [self objectAtIndex:index];
        if ([obj isKindOfClass:[NSNumber class]]){
            number = obj;
        }
    }
    
    if (!number) {
        number = [NSNumber numberWithInt:0];
    }
    return number;
}


- (NSDictionary*)safeDictionaryAtIndex:(NSUInteger)index {
    NSDictionary* dictionary = nil;
    
    if (index < self.count){
        id obj = [self objectAtIndex:index];
        if ([obj isKindOfClass:[NSDictionary class]]){
            dictionary = obj;
        }
    }
    
    if (!dictionary) {
        dictionary = [NSDictionary dictionary];
    }
    return dictionary;
}
- (id)safeObjectAtIndex:(NSUInteger)index{
    if (index < self.count){
        id obj = [self objectAtIndex:index];
        if (obj!=[NSNull null]) {
             return obj;
        }
    }
    return nil;
}
+ (NSArray*)arrayWithInts:(NSInteger)num count:(NSInteger)count{
    NSMutableArray *arrResult=[[NSMutableArray alloc]init];
    for (NSInteger i=num; i<count+num; i++) {        
        [arrResult addObject:[NSNumber numberWithInteger:i]];
    }
    return arrResult;
}
@end
