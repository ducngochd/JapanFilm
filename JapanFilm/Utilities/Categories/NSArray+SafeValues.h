

#import <Foundation/Foundation.h>

@interface NSArray (NSArray_SafeValues)

- (NSString*)safeStringAtIndex:(NSUInteger)index;
- (id)safeObjectAtIndex:(NSUInteger)index;
- (NSNumber*)safeNumberAtIndex:(NSUInteger)index;
- (NSDictionary*)safeDictionaryAtIndex:(NSUInteger)index;
+ (NSArray*)arrayWithInts:(NSInteger)num count:(NSInteger)count;

@end
