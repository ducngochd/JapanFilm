

#import "NSDictionary+SafeValues.h"

@implementation NSDictionary (NSDictionary_SafeValues)

- (NSString*)safeStringForKey:(id)key {
    NSString* string = nil;
    id obj = [self objectForKey:key];
    if ([obj isKindOfClass:[NSString class]]){
        string = obj;
    }
    else {
        string = @"";
    }
    return [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

- (NSNumber*)safeNumberForKey:(id)key {
    NSNumber* number = nil;
    id obj = [self objectForKey:key];
    if ([obj isKindOfClass:[NSNumber class]]){
        number = obj;
    }
    else {
        number = [NSNumber numberWithInt:0];
    }
    return number;
}

- (NSArray*)safeArrayForKey:(id)key {
    NSArray* array = nil;
    id obj = [self objectForKey:key];
    if ([obj isKindOfClass:[NSArray class]]){
        array = obj;
    }
    else {
        array = [NSArray array];
    }
    return array;
}

- (NSDictionary*)safeDictionaryForKey:(id)key {
    NSDictionary* dictionary = nil;
    id obj = [self objectForKey:key];
    if ([obj isKindOfClass:[NSDictionary class]]) {
        dictionary = obj;
    }
    else {
        dictionary = [NSDictionary dictionary];
    }
    return dictionary;
}

- (UIImage*)safeImageForKey:(id)key;
{
    UIImage* image = nil;
    id obj = [self objectForKey:key];
    if ([obj isKindOfClass:[UIImage class]]) {
        image = obj;
    }
    else {
        image = nil;
    }
    return image;
}
- (id)safeObjectForKey:(id)key{    
    id obj = [self objectForKey:key];
    if (obj!=[NSNull null]){        
        if ([obj isKindOfClass:[NSNumber class]]) {
            return [NSString stringWithFormat:@"%@",obj];
        }
        return obj;
    }
    return nil;
}
@end
