//
//  Utilities.h
//  SevenBillionRun
//
//  Created by トラストリング on 2018/06/14.
//  Copyright © 2018 com.trustring.7billionrun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, kAlertType) {
    kAlertTypeNotification = 0,
    kAlertTypeConfirm,
};

@interface Utilities : NSObject
#pragma mark - Get TIME
+ (long long)getNowTimestampSec;
+ (long long)getNowTimestampMesc;
+ (NSString *)getNowTimestampString;
+ (NSString *)getHMSFormatBySeconds:(int)seconds;


+ (NSString *)getDateByTimestamp:(long long)timestamp type:(NSInteger)timeType;
+ (NSString *)formatDatefromDate:(NSDate *)date type:(NSInteger)timeType;
+ (NSInteger)getTimestampByDate:(NSString *)dateString type:(NSInteger)timeType;

+ (NSString *)getDocDir;
+ (NSString *)getVideoDir;
+ (NSString *)getAudioDir;
+ (NSString *)getAudioFilePath;
+ (NSString *)getTempPicDir;

+ (UIImage *)getVideoImage:(NSURL *)videoPath;

+ (BOOL)isAudioRecordPermit;
+ (BOOL)isPhotoLibraryPermit;
+ (BOOL)isCameraPermit;

+ (void)writeImageToMUKAssetsGroup:(UIImage *)image completion:(void(^)(BOOL isSuccess))completion;
+ (void)writeVideoToMUKAssetsGroup:(NSURL *)videoURL completion:(void(^)(BOOL isSuccess))completion;

+ (UIImage *)fixOrientation:(UIImage *)aImage;
+ (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage;
+ (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize;





#pragma mark - show custom alert
+ (void)showAlertWithMessage:(NSString *)msg withType:(kAlertType)type inViewController:(id)viewcontroller completedWithString:(void(^)(NSString *btnString))completedWithBtnString;

@end
