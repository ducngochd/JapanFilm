//
//  JFFilmContentEntity.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/27.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "JFFilmContentEntity.h"

@implementation JFFilmContentEntity
- (instancetype)initWithDictionary:(NSDictionary*)dict{
    if (self = [super init]) {
        [self setComicContentModelWithDictionary:dict];
    }
    return self;
}

- (void)setComicContentModelWithDictionary:(NSDictionary*)dict{
    [self setValuesForKeysWithDictionary:dict];
}


+ (NSMutableArray*)modelArrayForDataArray:(NSArray*)dataArray{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dict in dataArray) {
        id model = [[[self class] alloc]initWithDictionary:dict];
        [array addObject:model];
    }
    return array;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.id_part = [value integerValue];
    }
}
@end
