//
//  JFObjectResponse.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/26.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "JFObjectResponse.h"

@implementation JFObjectResponse
+ (instancetype)modelWithDictionary:(NSDictionary *)dict {
    return [[self alloc]initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    if (self = [super init]) {
        self.status     = [[dict objectForKey:@"status"] boolValue];
        self.message    = [dict objectForKey:@"message"];
        NSMutableArray *tempList = [NSMutableArray array];
        if ([[dict objectForKey:@"data"] isKindOfClass:[NSArray class]]) {
            NSArray *temp = [dict objectForKey:@"data"];
            for (NSDictionary *dict  in temp) {
                JFFilmEntity *film = [JFFilmEntity modelWithDictionary:dict];
                [tempList addObject:film];
            }
        }
        self.listObject = [tempList copy];
    }
    return self;
}
@end
