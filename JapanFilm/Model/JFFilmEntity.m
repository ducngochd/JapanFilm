//
//  JFPlayEntity.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/18.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import "JFFilmEntity.h"

@implementation JFFilmEntity
+ (instancetype)modelWithDictionary:(NSDictionary *)dict {
    return [[self alloc]initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    if (self = [super init]) {
        self.film_id = [[dict objectForKey:@"id"] integerValue];
        self.name = [dict objectForKey:@"name"];
        self.actor = [dict objectForKey:@"actor"];
        self.info = [dict objectForKey:@"info"];
        self.director = [dict objectForKey:@"director"];
        self.time = [dict objectForKey:@"time"];
        self.year = [dict objectForKey:@"year"];
        self.area = [dict objectForKey:@"area"];
        self.rate = [[dict objectForKey:@"rate"] integerValue];
        self.thumbnail = [dict objectForKey:@"thumbnail"];
        self.count = [[dict objectForKey:@"count"] integerValue];
        
        
    }
    return self;
}

- (instancetype)initWithDict:(NSDictionary*)dict
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (NSMutableArray*)modelArrayForDataArray:(NSArray*)dataArray{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dict in dataArray) {
        id model = [[[self class] alloc]initWithDict:dict];
        [array addObject:model];
    }
    return array;
}
@end
