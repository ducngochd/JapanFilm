//
//  JFObjectResponse.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/26.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JFFilmEntity.h"

@interface JFObjectResponse : NSObject
@property (nonatomic, assign) BOOL status;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSArray *listObject;

+ (instancetype)modelWithDictionary:(NSDictionary *)dict;

@end
