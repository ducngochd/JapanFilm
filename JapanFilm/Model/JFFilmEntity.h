//
//  JFPlayEntity.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/18.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <Foundation/Foundation.h>
@class JFFilmContentEntity;

@interface JFFilmEntity : NSObject
@property (nonatomic, assign) NSInteger film_id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *part;
@property (nonatomic, strong) NSString *actor;
@property (nonatomic, strong) NSString *info;
@property (nonatomic, strong) NSString *director;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *year;
@property (nonatomic, strong) NSString *area;
@property (nonatomic, assign) NSInteger rate;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, strong) NSString *thumbnail;

@property (nonatomic, strong) NSMutableArray *contentArray;


+ (instancetype)modelWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;

+ (NSMutableArray*)modelArrayForDataArray:(NSArray*)dataArray;
@end
