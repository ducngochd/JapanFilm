//
//  JFFilmContentEntity.h
//  JapanFilm
//
//  Created by トラストリング on 2018/07/27.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JFFilmContentEntity : NSObject
@property (nonatomic, assign) NSInteger id_part;
@property (nonatomic, assign) NSInteger id_film;
@property (nonatomic, strong) NSString *thumbnail;

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *viewed;
@property (nonatomic, strong) NSString *viewed_day;

+ (NSMutableArray*)modelArrayForDataArray:(NSArray*)dataArray;

@end
