//
//  main.m
//  JapanFilm
//
//  Created by トラストリング on 2018/07/14.
//  Copyright © 2018 com.vdngoc.diembao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
