//
//  CWFlowLayout.h
//  CWCarousel
//
//  Created by WangChen on 2018/4/3.
//  Copyright © 2018年 ChenWang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, CWCarouselStyle) {
    CWCarouselStyle_Unknow = 0,     
    CWCarouselStyle_Normal,
    CWCarouselStyle_H_1,
    CWCarouselStyle_H_2,
    CWCarouselStyle_H_3,
};

@interface CWFlowLayout : UICollectionViewFlowLayout
/**
 影响轮播图风格
 */
@property (nonatomic, assign) CWCarouselStyle style;

/**
 * 横向滚动时,每张轮播图之间的间距
 * CWCarouselStyle_H_3 样式时设置无效
 */
@property (nonatomic, assign) CGFloat itemSpace_H;

/**
 * 横向滚动时,每张轮播图的宽度
 * style = CWCarouselStyle_Normal 时设置无效
 */
@property (nonatomic, assign) CGFloat itemWidth;

/**
 * style = CWCarouselStyle_H_2 有效
 * 前后2张图的缩小比例 (0.0 ~ 1.0)
 * 默认: 0.8
 */
@property (nonatomic, assign) CGFloat minScale;

/**
 * style = CWCarouselStyle_H_3 有效
 * 中间一张图放大比例
 * 默认: 1.2
 */
@property (nonatomic, assign) CGFloat maxScale;

/**
 纵向滚动时,每张轮播图之间的间距(暂未实现)
 */
@property (nonatomic, assign) CGFloat itemSpace_V;


/**
 构造方法

 @param style 轮播图风格
 @return 实例对象
 */
- (instancetype)initWithStyle:(CWCarouselStyle)style;
@end
